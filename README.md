# Blue Scheme

Extensible typescript/javascript object validation.

## Quick Start

**Instalation**

```bash
npm i blue-scheme
```

**Basic Usage**

```typescript
import { Schema, isA, FieldType as FT } from "blue-scheme";

export const UserSchema = new Schema({
    username: isA(FT.string, {
        min: 1,
        max: 50,
        regex: /^[A-Za-z0-9\-]+$/
    }),
    password: isA(FT.string, {
        min: 1,
        max: 100,
    }),
    email: isA(FT.string, {
        optional: true,
        min: 3,
        max: 150,
        regex: /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
    }),
    address: new Schema({
        number: isA(FT.number);
        street: isA(FT.string);
        zip: isA(FT.number);
    })
});


const result = UserSchema.validate({
    username: "drew",
    password: "security",
    address: {
        number: 4000,
        street: "Apple",
        zip: 86741
    }
});

console.log(result.isValid) // true
```

## API Documentation

Available [here](https://youngwerth.gitlab.io/blue-scheme)