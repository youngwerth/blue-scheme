
export type InvalidField = { name: string, result: ValidatorResult };

export interface ValidatorResult {
    readonly isValid: boolean;
    readonly message: string;
    readonly hasFields: boolean;
    getInvalidFields(): InvalidField[];
}

export interface Validator {
    hasLabel(): boolean;
    setLabel(label: string): void;
    validate(value: any): ValidatorResult;
}

export enum FieldType {
    any = "any",
    string = "string",
    number = "number",
    boolean = "boolean",
    object = "object",
    stringArray = "string[]",
    numberArray = "number[]",
    booleanArray = "boolean[]",
    objectAarray = "object[]"
}

export type FieldOpts = {
    label?: string,
    optional?: boolean,
    min?: number,
    max?: number,
    minSize?: number,
    maxSize?: number,
    allowedValues?: any[],
    regex?: RegExp,
    custom?: (value: any) => ValidatorResult
};
