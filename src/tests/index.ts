/// </// <reference path="mocha" />
import assert = require("assert");
import { isA, Schema, FieldType as Ty } from "../index";

describe("Blue Schema", function() {
    describe("isA", function() {
        it("Validates a string", function() {
            const test = isA(Ty.string);

            assert.equal(test.validate("hello").isValid, true);
            assert.equal(test.validate(5).isValid, false);
        });

        it("Validates a string min/max length", function() {
            const test = isA(Ty.string, {
                min: 4,
                max: 10
            });

            assert.equal(test.validate("hello").isValid, true);
            assert.equal(test.validate(5).isValid, false);
            assert.equal(test.validate("12").isValid, false);
            assert.equal(test.validate("12jfdhskjfhkdsjhfks").isValid, false);
        });

        it("Validates a string against regex", function() {
            const test = isA(Ty.string, {
                regex: /[A-Za-z0-9]+/g,
                optional: true
            });

            assert.equal(test.validate("H12hello").isValid, true);
            assert.equal(test.validate("%^&*^(*").isValid, false);
        });

        it("Validates an optional string", function() {
            const test = isA(Ty.string, {
                optional: true
            });

            assert.equal(test.validate("hello").isValid, true);
            assert.equal(test.validate(undefined).isValid, true);
        });

        it("Validates a number", function() {
            const test = isA(Ty.number);

            assert.equal(test.validate("hello").isValid, false);
            assert.equal(test.validate(5).isValid, true);
        });

        it("Validates an object", function() {
            const test = isA(Ty.object);

            assert.equal(test.validate({}).isValid, true);
            assert.equal(test.validate(5).isValid, false);
        });

        it("Validates a boolean", function() {
            const test = isA(Ty.boolean);

            assert.equal(test.validate(true).isValid, true);
            assert.equal(test.validate(5).isValid, false);
        });
    });

    describe("Schema", function() {
        it("Validates a document", function() {
            const schema = new Schema({
                username: isA(Ty.string),
                password: isA(Ty.string),
                email: isA(Ty.string, { optional: true }),
                address: isA(Ty.string)
            });

            assert.equal(schema.validate({
                username: "Drew",
                password: "bjafskdjhfa",
                email: "Hello World",
                address: "5476896547 jrhdfsk 49349857"
            }).isValid, true);

            assert.equal(schema.validate({
                username: "Drew",
                password: "bjafskdjhfa",
                address: "5476896547 jrhdfsk 49349857"
            }).isValid, true);

            assert.equal(schema.validate({
                password: "bjafskdjhfa",
                address: "5476896547 jrhdfsk 49349857"
            }).isValid, false);

            assert.equal(schema.validate({
                username: "Drew",
                password: 5,
                address: "5476896547 jrhdfsk 49349857"
            }).isValid, false);
        });
    });
})