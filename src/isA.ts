import { Validator, FieldType, ValidatorResult, FieldOpts } from "./definitions";
import { ValidationResult } from "./validationResult";

type Primitive = "string" | "object" | "number" | "boolean";

interface LengthWise { length: number; }

function fieldResult(isValid: boolean, message: string): ValidationResult {
    return new ValidationResult({
        isValid, message,
        invalidFields: [],
        hasFields: false,
    });
}

function isIn(item: any, array: any[]) {
    return array.indexOf(item) !== -1;
}

class BasicFieldValidator {
    protected opts: FieldOpts;

    constructor(opts?: FieldOpts) {
        this.opts = opts || {};
    }

    protected getLabel() {
        return this.opts.label || "field";
    }

    hasLabel(): boolean {
        return !!this.opts.label;
    }

    setLabel(label: string) {
        this.opts.label = label;
    }
}

class StringValidator extends BasicFieldValidator implements Validator {
    validate(value: any): ValidatorResult {
        if(!!this.opts.optional && (typeof value === "undefined" || value === ""))
            return fieldResult(true, `${this.getLabel()} is valid`);
        if (!(typeof value === "string"))
            return fieldResult(false, `${this.getLabel()} is not a string.`);
        if (!!this.opts.min && (value.length < this.opts.min))
            return fieldResult(false, `${this.getLabel()} does not have enough characters.`);
        if (!!this.opts.max && (value.length > this.opts.max))
            return fieldResult(false, `${this.getLabel()} has too many characters.`);
        if (!!this.opts.regex && typeof value === "string" && !this.opts.regex.test(value))
            return fieldResult(false, `${this.getLabel()} does not pass the regex test`);
        if (!!this.opts.allowedValues && !isIn(value, this.opts.allowedValues))
            return fieldResult(false, `${this.getLabel()} is not one of the allowed values`);
        return fieldResult(true, `${this.getLabel()} is valid`);
    }
}

class NumberValidator extends BasicFieldValidator implements Validator {
    validate(value: any): ValidatorResult {
        if(!!this.opts.optional && typeof value === "undefined")
            return fieldResult(true, `${this.getLabel()} is valid`);
        if (!(typeof value === "number"))
            return fieldResult(false, `${this.getLabel()} is not a valid number`);
        if (!!this.opts.min && (value < this.opts.min))
            return fieldResult(false, `${this.getLabel()} is to small.`);
        if (!!this.opts.max && (value > this.opts.max))
            return fieldResult(false, `${this.getLabel()} is too big.`);
        if (!!this.opts.allowedValues && !isIn(value, this.opts.allowedValues))
            return fieldResult(false, `${this.getLabel()} is not one of the allowed values`);
        return fieldResult(true, `${this.getLabel()} is valid`);
    }
}

class BooleanValidator extends BasicFieldValidator implements Validator {
    validate(value: any): ValidatorResult {
        if(!!this.opts.optional && typeof value === "undefined")
            return fieldResult(true, `${this.getLabel()} is valid`);
        if(!(typeof value === "boolean"))
            return fieldResult(false, `${this.getLabel()} is not a valid boolean`);
        return fieldResult(true, `${this.getLabel()} is not a valid boolean`);
    }
}

class ObjectValidator extends BasicFieldValidator implements Validator {
    validate(value: any): ValidatorResult {
        if(!!this.opts.optional && typeof value === "undefined")
            return fieldResult(true, `${this.getLabel()} is valid`);
        if(!(typeof value === "object"))
            return fieldResult(false, `${this.getLabel()} is not a valid object`);
        return fieldResult(true, `${this.getLabel()} is not a valid boolean`);
    }
}

class AnyValidator extends BasicFieldValidator implements Validator {
    validate(value: any): ValidatorResult {
        if (!!this.opts.custom)
            return this.opts.custom(value);
            return fieldResult(true, `${this.getLabel()} is valid`);
    }
}

class BasicArrayFieldValidator extends BasicFieldValidator {
    protected arrayValidator(type: Primitive, value: any): ValidatorResult {
        if(!!this.opts.optional && typeof value === "undefined")
            return fieldResult(true, `${this.getLabel()} is valid`);

        if (!(value instanceof Array))
            return fieldResult(false, `${this.getLabel()} is not an array`);

        if(!!this.opts.minSize && !(value.length < this.opts.minSize))
            return fieldResult(false, `${this.getLabel()} is too small`);

        if(!!this.opts.maxSize && !(value.length > this.opts.maxSize))
            return fieldResult(false, `${this.getLabel()} is too big`);

        for (const item of value) {
            if (!(typeof value === type))
                return fieldResult(false, `${this.getLabel()} contains an invalid value`);

            if (typeof value === "string") {
                if (!!this.opts.min && ((value as string).length < this.opts.min))
                    return fieldResult(false, `${this.getLabel()} does not have enough characters.`);
                if (!!this.opts.max && ((value as string).length > this.opts.max))
                    return fieldResult(false, `${this.getLabel()} has too many characters.`);
                if (!!this.opts.allowedValues && !isIn(value, this.opts.allowedValues))
                    return fieldResult(false, `${this.getLabel()} is not one of the allowed values`);
                if (!!this.opts.regex && typeof value === "string" && !this.opts.regex.test(value))
                    return fieldResult(false, `${this.getLabel()} does not pass the regex test`);
            }

            if (typeof value === "number") {
                if (!!this.opts.min && (value < this.opts.min))
                    return fieldResult(false, `${this.getLabel()} is to small.`);
                if (!!this.opts.max && (value > this.opts.max))
                    return fieldResult(false, `${this.getLabel()} is too big.`);
                if (!!this.opts.allowedValues && !isIn(value, this.opts.allowedValues))
                    return fieldResult(false, `${this.getLabel()} is not one of the allowed values`);
            }
        }

        return fieldResult(true, `${this.getLabel()} is valid`);
    }
}

class StringArrayValidator extends BasicArrayFieldValidator implements Validator {
    validate(value: any): ValidatorResult {
        return this.arrayValidator("string", value);
    }
}

class NumberArrayValidator extends BasicArrayFieldValidator implements Validator {
    validate(value: any): ValidatorResult {
        return this.arrayValidator("number", value);
    }
}

class ObjectArrayValidator extends BasicArrayFieldValidator implements Validator {
    validate(value: any): ValidatorResult {
        return this.arrayValidator("object", value);
    }
}

class BooleanArrayValidator extends BasicArrayFieldValidator implements Validator {
    validate(value: any): ValidatorResult {
        return this.arrayValidator("boolean", value);
    }
}

export function isA(fieldType: FieldType, opts?: FieldOpts): Validator {
    if (!!opts && !!opts.custom)
        return new AnyValidator(opts);

    switch (fieldType) {
        case FieldType.any:
            return new AnyValidator(opts);
        case FieldType.string:
            return new StringValidator(opts);
        case FieldType.number:
            return new NumberValidator(opts);
        case FieldType.boolean:
            return new BooleanValidator(opts);
        case FieldType.object:
            return new ObjectValidator(opts);
        case FieldType.stringArray:
            return new StringArrayValidator(opts);
        case FieldType.numberArray:
            return new NumberArrayValidator(opts);
        case FieldType.booleanArray:
            return new BooleanArrayValidator(opts);
        case FieldType.objectAarray:
            return new ObjectArrayValidator(opts);
        default:
            throw new Error("Invalid field type");
    }
}
