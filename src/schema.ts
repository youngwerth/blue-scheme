import { Validator, InvalidField } from "./definitions";
import { ValidationResult } from "./validationResult";

export class Schema implements Validator {
    private readonly fields: { [fieldName: string]: Validator };
    private label?: string;

    constructor(fields: { [fieldName: string]: Validator }, label?: string) {
        this.fields = fields;
        this.label = label;
    }

    private noObjectResult(): ValidationResult {
        const invalidFields: InvalidField[] = [];
        for (const field in this.fields) {
            invalidFields.push({
                name: field,
                result: this.fields[field].validate(undefined)
            });
        }
        return new ValidationResult({
            isValid: false,
            message: "Not a document",
            hasFields: true,
            invalidFields
        });
    }

    hasLabel(): boolean {
        return !!this.label;
    }

    setLabel(label: string) {
        this.label = label;
    }

    validate(document: any): ValidationResult {
        let isValid = true;
        let message = `${this.label || "Schema"} is valid.`;
        const invalidFields: InvalidField[] = [];
        if (document instanceof Object) {
            for (const field in this.fields) {
                const result = this.fields[field].validate(document[field]);
                if (!result.isValid) {
                    isValid = false;
                    invalidFields.push({ name: field, result });
                }
            }

            if (!isValid) {
                message = `${this.label || "Schema"} has invalid fields.`;
            }

            return new ValidationResult({ hasFields: true, isValid, message, invalidFields});
        }

        return this.noObjectResult();
    }
}