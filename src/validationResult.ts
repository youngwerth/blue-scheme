import { ValidatorResult, InvalidField} from "./definitions";

export class ValidationResult implements ValidatorResult {
    private readonly invalidFields: InvalidField[];
    readonly isValid: boolean;
    readonly message: string;
    readonly hasFields: boolean = true;

    constructor(opts: {
        isValid: boolean,
        message: string,
        hasFields: boolean;
        invalidFields: InvalidField[]
    }) {
        this.isValid = opts.isValid;
        this.message = opts.message;
        this.hasFields = opts.hasFields;
        this.invalidFields = opts.invalidFields;
    }

    getInvalidFields(): InvalidField[] {
        return this.invalidFields;
    };
}